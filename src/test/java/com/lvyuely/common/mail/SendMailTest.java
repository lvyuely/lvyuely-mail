package com.lvyuely.common.mail;

import org.junit.Test;

import com.lvyuely.common.mail.Email.ContentType;

public class SendMailTest {
	@Test
	public void send() {
		Config config = new Config();
		config.setAuth(true);
		config.setUsername("lvyuely@yeah.net");
		config.setPassword("");
		config.setSmtp("smtp.yeah.net");
		config.setProxy(false);
		config.setProxyHost("10.100.120.12");
		config.setProxyPort(8880);
		MailManager manager = new MailManager(config);
		Email mail = new Email();
		mail.setSender("lvyuely@yeah.net");
		mail.setAddressees(new String[] { "lvyue@litsoft.com.cn" });
		mail.setBcc(new String[] { "596688499@qq.com" });
		mail.setSubject("模板修改");
		mail.setContent("你好:\n  请根据附件中的模板配置对reasion.php进行修改！！！");
		// mail.setContent("<!DOCTYPE html><html lang=\"zh-CN\"><head><title>您的礼品已收到</title><meta charset=\"utf-8\" /></head><body>您的礼品我们已收到，礼品号33001;请点击下方连接查看您的礼品投递情况.<br /><a href=\"http://www.baidu.com?d=33001\">http://www.baidu.com?d=33001<a></body></html>");
		mail.setContentType(ContentType.HTML);
		mail.setAffixName(new String[] { "D:/reasion.php", "D:/模板配置.txt" });
		manager.send(mail);
	}
}
